#!/bin/bash

command -v tmux >/dev/null 2>&1 || { printf "Aborting." >&2; exit 1; }

SESSION=0

# if the session is already running, just attach to it.
tmux has-session -t $SESSION
if [ $? -eq 0 ]; then
  echo "Session $SESSION already exists. Attaching."
  sleep 1
  tmux attach -t $SESSION
  exit 0;
fi
if [ -e Chatbox/params.usr ]
then
	if [ -r Chatbox/params.usr ]
	then
		
		tchat=$(cut -d= -f2 Chatbox/params.usr)
		if [ -e $tchat ]
		then
			
		else
			echo "DEFAULT START" > /tmp/default
			chmod 666 /tmp/default
		fi
	else
		
	fi
else
	
fi
# create a new session, named $SESSION, and detach from it
tmux  new-session -d -s $SESSION

# Create windows
tmux set-option -g base-index 1
tmux set-window-option -t $SESSION -g automatic-rename off
tmux set-window-option -g pane-base-index 0

tmux new-window -t $SESSION:0 -k -n chatbox-
tmux split-window -v -p 20
tmux split-window -h -p 25
tmux send-keys -t ${window}.0 'tail -f '$tchat Enter
tmux send-keys -t ${window}.1 'cd Chatbox' Enter
tmux send-keys -t ${window}.1 './chatbox.sh' Enter
tmux set-window-option -g clock-mode-colour white
tmux clock-mode -t 2
tmux select-pane -t 1
tmux set-window-option -t $SESSION:0 automatic-rename off

# Selecting and attaching
tmux select-window -t $SESSION:0
tmux attach -t $SESSION