#!/bin/bash
#Some stuff I might use :
#if [ ! -e params.usr ]
#then
#"Kevin=/tmp/default" >> params.usr
#fi


startchat () {
	#start of messaging loop
	echo "$1 connected" >> $2
	echo "To stop messaging send an empty message"
	while :
	do
	read msg
			if [ "$msg" = "" ]
			then
				echo "$1 disconnected" >> $2
				break
			else
				echo "$1 : $msg" >> $2
			fi
	done
	}


echo "Welcome to chatbox"
echo "Loading params.usr"
if [ -e params.usr ]
then
	if [ -r params.usr ]
	then
		usr=$(cut -d= -f1 params.usr)
		tchat=$(cut -d= -f2 params.usr)
		echo "Detected username : $usr"
		if [ -e $tchat ] && [ -w $tchat ]
		then
			echo "Connected to chatbox $tchat"
			startchat $usr $tchat
		else
			echo "Could not access chatbox $tchat"
		fi
	else
		echo "No read access on params.usr"
	fi
else
	echo "Could not find params.usr"
fi
echo "-END-"

